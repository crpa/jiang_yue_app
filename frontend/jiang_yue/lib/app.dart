//app主页
import 'package:flutter/material.dart';
import 'package:jiang_yue/home/AddFriend.dart';
//import './personal/personal.dart'; // 个人页面
import 'package:jiang_yue/home/PostPage.dart';
import 'package:jiang_yue/home/PublishPost.dart';
import 'package:jiang_yue/home/Toast.dart';
import 'package:jiang_yue/page/MyInfoPage.dart';
import 'package:jiang_yue/page/SearchPage.dart';
//import 'package:jiang_yue/page/UserInfoPage.dart';  //测试修改用户信息页
import 'dart:convert';

import 'package:jiang_yue/utils/cache/UserCache.dart';

class App extends StatefulWidget {
  @override
  MainState createState() => MainState();
}

class MainState extends State<App> {
  var _currentIndex = 0;

  currentPage() {
    switch (_currentIndex) {
      case 0:
        return PostPage();

      case 1:
        return MyInfoPage();
    }
  }

  _popupMenuItem({String title, @required String verifyInfo, String imagePath, IconData icon}) {
    return PopupMenuItem(
      child: GestureDetector(
        onTap: () {
          if (userInfo.length == 0) {
            Toast.show(context, '您未登陆，请登录重试');
            return;
          }
          switch (verifyInfo) {
            case 'pulishPost':
              Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new PublishPost()));
              break;
            case 'addFriend':
              Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new AddFriendPage()));
              break;
            default:
          }
        },
        child: Row(
          children: <Widget>[
            imagePath != null
                ? Image.asset(
                    imagePath,
                    width: 32.0,
                    height: 32.0,
                  )
                : SizedBox(
                    width: 32.0,
                    height: 32.0,
                    child: Icon(
                      icon,
                      color: Colors.white,
                    ),
                  ),
            Container(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                title,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
//右上角+号处

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('江约'),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new SearchPage()));
            },
            child: Icon(
              Icons.search,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30.0, right: 20.0),
            child: GestureDetector(
              onTap: () {
                showMenu(
                  context: context,
                  color: Colors.blue,
                  position: RelativeRect.fromLTRB(500.0, 76.0, 10.0, 0.0),
                  items: <PopupMenuEntry>[
                    _popupMenuItem(title: '发布帖子', verifyInfo: 'pulishPost', icon: Icons.add_comment),
                    _popupMenuItem(title: '添加朋友', verifyInfo: 'addFriend', imagePath: 'images/icon_menu_addfriend.png'),
                  ],
                );
              },
              child: Icon(Icons.add),
            ),
          ),
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: ((index) {
          setState(() {
            _currentIndex = index;
          });
        }),
        items: [
          new BottomNavigationBarItem(
              title: new Text(
                '首页',
                style: TextStyle(color: _currentIndex == 0 ? Color(0xFF46c01b) : Color(0xff999999)),
              ),
              icon: _currentIndex == 0
                  ? Image.asset(
                      'images/weixin_pressed.png',
                      width: 32.0,
                      height: 28.0,
                    )
                  : Image.asset(
                      'images/weixin_normal.png',
                      width: 32.0,
                      height: 28.0,
                    )),
          new BottomNavigationBarItem(
              title: new Text(
                '我的',
                style: TextStyle(color: _currentIndex == 1 ? Color(0xFF46c01b) : Color(0xff999999)),
              ),
              icon: _currentIndex == 1
                  ? Image.asset(
                      'images/profile_pressed.png',
                      width: 32.0,
                      height: 28.0,
                    )
                  : Image.asset(
                      'images/profile_normal.png',
                      width: 32.0,
                      height: 28.0,
                    )),
        ],
      ),
      body: currentPage(),
    );
  }
}
