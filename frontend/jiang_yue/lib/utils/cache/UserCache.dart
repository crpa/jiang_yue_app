import 'package:flutter/cupertino.dart';
import 'package:jiang_yue/home/Toast.dart';

class UserInfo {
  UserInfo({
    @required this.id,
    @required this.username,
    this.decoration,
    this.nickname,
    this.avatorURL,
    this.creditWorthiness, 
  });

  final int id; // 用户ID
  final String username; // 用户名（登录所用）
  String decoration; // 用户自身的描述（简介）
  final String nickname; // 昵称
  String avatorURL; // 头像 
  int creditWorthiness;//信誉积分
  // List<int> likeArticleList; // 喜欢文章的列表ID
  // List<int> publishArticleList; // 发布的文章列表ID
  // List<int> browseArticleList; // 浏览过的文章列表ID
  // List<int> friendList;//好友列表
  // List<int> messageFriendList; // 消息好友列表（User列表ID）    注：该列表为某个用户向其他用户发送的添加好友请求
}

List userInfo = [];