// 数据类

List articleInfos = []; // 帖子列表

var articleShow; // 帖子展示

List commentList = []; // 评论列表（查看帖子所用）

List friendList = []; // 查询朋友列表

List mycommentList = [];///我的评论列表

List browseList = [];//浏览过的帖子列表

List publishList = [];//发布的帖子列表

List joinList = [];//参加的活动列表

List posts = [];  //postlistview中的列表

List evaluateList=[]; // 参加活动的成员信息

List starList=[]; // 评价的星级列表

var userMsg; // 用户基本信息

List userPushList=[]; // 用户基本信息的发布帖子列表

List messageList = []; //消息列表

List commentMsgList=[]; // 评论消息列表

List myFriendList = []; //我的好友列表

getArticleTitle(int articleID) {
  var article;
  articleInfos.forEach((articleItem) {
    if (articleItem['postID'] == articleID) {
      article = articleItem;
    }
  });

  return article;
}

getIndex(int articleID){
  int index=0;
  int idx;
  articleInfos.forEach((articleItem) {
    if (articleItem['postID'] == articleID) {
      idx=index;
    }
    index++;
  });

  return idx;
}