// 评论数据模型

import 'package:flutter/cupertino.dart';

class Comment {
  Comment({
    @required this.id,
    @required this.content,
    @required this.authorID,
    int this.article,
  });

  final int id; // 评论ID
  final String content; // 评论内容
  final int authorID; // 评论作者ID
  int article; // 评论的帖子ID
}

// 通过文章ID获取关于该文章的评论
List<Comment> getCommentList(int articleID) {
  List<Comment> commentList = [];

  comments.forEach((commentItem) {
    if (commentItem.article == articleID) {
      commentList.add(commentItem);
    }
  });

  return commentList;
}

// 转换毫秒数为时间，请使用common_utils库

final List<Comment> comments = [
  // Comment(
  //   id: 0,
  //   content: '0',
  //   authorID: 0,
  //   article: 1,
  // ),
  // Comment(
  //   id: 1,
  //   content: '1',
  //   authorID: 1,
  //   article: 2,
  // ),
  // Comment(
  //   id: 2,
  //   content: '2',
  //   authorID: 2,
  //   article: 5,
  // ),
  // Comment(
  //   id: 3,
  //   content: '3',
  //   authorID: 3,
  //   article: 0,
  // ),
  // Comment(
  //   id: 4,
  //   content: '4',
  //   authorID: 4,
  //   article: 9,
  // ),
  // Comment(
  //   id: 5,
  //   content: '5',
  //   authorID: 6,
  //   article: 3,
  // ),
  // Comment(
  //   id: 6,
  //   content: '6',
  //   authorID: 1,
  //   article: 4,
  // ),
  // Comment(
  //   id: 7,
  //   content: '7',
  //   authorID: 4,
  //   article: 6,
  // ),
  // Comment(
  //   id: 8,
  //   content: '8',
  //   authorID: 5,
  //   article: 1,
  // ),
  // Comment(
  //   id: 9,
  //   content: '9',
  //   authorID: 2,
  //   article: 7,
  // ),
  // Comment(
  //   id: 10,
  //   content: '10',
  //   authorID: 9,
  //   article: 3,
  // ),
  // Comment(
  //   id: 11,
  //   content: '11',
  //   authorID: 1,
  //   article: 8,
  // ),
  // Comment(
  //   id: 12,
  //   content: '12',
  //   authorID: 3,
  //   article: 6,
  // ),
  // Comment(
  //   id: 13,
  //   content: '13',
  //   authorID: 8,
  //   article: 2,
  // ),
  // Comment(
  //   id: 14,
  //   content: '14',
  //   authorID: 7,
  //   article: 3,
  // )
];
