// 用户数据模型

import 'package:flutter/cupertino.dart';

class User {
  User({
    @required this.id,
    @required this.username,
    @required this.password,
    this.decoration,
    this.nickname,
    this.avatorURL,
    this.isLogin,
    this.likeArticleList,
    this.publishArticleList,
    this.browseArticleList,
    this.friendList,
    this.messageFriendList,
    this.creditWorthiness = 100, 
    this.focusList,
  });

  final int id; // 用户ID
  final String username; // 用户名（登录所用）
  final String password; // 密码（登录所用）
  String decoration; // 用户自身的描述（简介）
  final String nickname; // 昵称
  String avatorURL; // 头像
  bool isLogin; // 是否登录
  List<int> likeArticleList; // 喜欢文章的列表ID
  List<int> publishArticleList; // 发布的文章列表ID
  List<int> browseArticleList; // 浏览过的文章列表ID
  List<int> friendList; // 好友列表（User列表ID）
  List<int> messageFriendList; // 消息好友列表（User列表ID）    注：该列表为某个用户向其他用户发送的添加好友请求
  int creditWorthiness;
  List<int> focusList; // 用户关注的其他用户列表ID

  // 点击喜欢时向喜欢的文章列表中增加该文章ID
  addLikeArticle(int articleID) {
    likeArticleList.add(articleID);
  }

  // 当用户浏览时向browseArticleList增加文章ID
  addBrowseArticleList(int articleID) {
    if (this.browseArticleList.contains(articleID)) {
      // 说明该帖子被浏览过
    } else {
      browseArticleList.add(articleID);
    }
  }

  // 当用户发布新文章时向publishArticleList增加文章ID
  addPublishArticleList(int articleID) {
    publishArticleList.add(articleID);
  }

  // 当用户发送好友请求后，向messageFriendList添加用户ID
  bool addMessageFriendList(int userID) {
    if (this.messageFriendList.contains(userID)) {
      // 说明已经邀请过
      return false;
    } else {
      messageFriendList.add(userID);
      return true;
    }
  }

  // 当用户同意其他用户发送的好友请求。删减messageFriendList的用户ID，同时向friendList添加对应用户ID
  removeMessageFriendList(int userID) {
    this.messageFriendList.remove(userID);
  }

  bool addFriendList(userID) {
    this.friendList.add(userID);
    return true;
  }
}

// 获取当前已登录的用户信息
User getIsLoginUser() {
  User user;
  users.forEach((userItem) {
    if (userItem.isLogin) {
      user = userItem;
    }
  });
  return user;
}

// 通过userID获取user
User getUser(int userID) {
  User user;
  users.forEach((userItem) {
    if (userItem.id == userID) {
      user = userItem;
      return;
    }
  });

  return user;
}

// 获得某一用户被点赞数
int getLikeNum(int userID) {
  int likeNum = 0;
  User user = getUser(userID);
  users.forEach((userItem) {
    user.publishArticleList.forEach((publishItem) {
      if (userItem.likeArticleList.contains(publishItem)) {
        likeNum++;
      }
    });
  });

  return likeNum;
}

List<User> users = [
  User(
    id: 0,
    username: 'youchen',
    password: '123456',
    decoration: '前端工程师',
    nickname: '官方账号',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false, // 表示该账号已登录
    likeArticleList: [0, 2, 5],
    publishArticleList: [0],
    browseArticleList: [1, 5, 7],
    friendList: [1, 2, 3, 6],
    messageFriendList: [1, 3, 5],
  ),
  User(
    id: 1,
    username: 'xxx1',
    password: '123456',
    decoration: '后端工程师',
    nickname: '用户1',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false,
    likeArticleList: [1],
    publishArticleList: [1],
    browseArticleList: [1, 3, 7],
    friendList: [],
    messageFriendList: [],
  ),
  User(
    id: 2,
    username: 'xxx2',
    password: '123456',
    decoration: '种花老头',
    nickname: '用户2',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false,
    likeArticleList: [2],
    publishArticleList: [2],
    browseArticleList: [1, 5, 7],
    friendList: [],
    messageFriendList: [],
  ),
  User(
    id: 3,
    username: 'xxx3',
    password: '123456',
    decoration: 'python工程师',
    nickname: '用户3',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false,
    likeArticleList: [3],
    publishArticleList: [3],
    browseArticleList: [1, 5, 7],
    friendList: [],
    messageFriendList: [],
  ),
  User(
    id: 4,
    username: 'xxx4',
    password: '123456',
    decoration: '前端工程师',
    nickname: '用户4',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false,
    likeArticleList: [4],
    publishArticleList: [4],
    browseArticleList: [1, 5, 7],
    friendList: [],
    messageFriendList: [],
  ),
  User(
    id: 5,
    username: 'xxx5',
    password: '123456',
    decoration: '前端工程师',
    nickname: '用户5',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false,
    likeArticleList: [5],
    publishArticleList: [5],
    browseArticleList: [1, 5, 7],
    friendList: [],
    messageFriendList: [],
  ),
  User(
    id: 6,
    username: 'xxx6',
    password: '123456',
    decoration: '龙组',
    nickname: '用户6',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false,
    likeArticleList: [6],
    publishArticleList: [6],
    browseArticleList: [1, 5, 7],
    friendList: [],
    messageFriendList: [],
  ),
  User(
    id: 7,
    username: 'xxx7',
    password: '123456',
    decoration: '前端工程师',
    nickname: '用户7',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false,
    likeArticleList: [7],
    publishArticleList: [7],
    browseArticleList: [1, 5, 7],
    friendList: [],
    messageFriendList: [],
  ),
  User(
    id: 8,
    username: 'xxx8',
    password: '123456',
    decoration: '前端工程师',
    nickname: '用户8',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false,
    likeArticleList: [8],
    publishArticleList: [8],
    browseArticleList: [1, 5, 7],
    friendList: [],
    messageFriendList: [],
  ),
  User(
    id: 9,
    username: 'xxx9',
    password: '123456',
    decoration: '前端工程师',
    nickname: '用户9',
    avatorURL: 'images/profilePhoto.jpg',
    isLogin: false,
    likeArticleList: [9],
    publishArticleList: [9],
    browseArticleList: [1, 5, 7],
    friendList: [],
    messageFriendList: [],
  ),
];
