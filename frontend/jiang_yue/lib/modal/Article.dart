// 文章数据模型

class Article {
  Article({
    this.id,
    this.title,
    this.authorID,
    this.mainAcrticle,
    this.createTime,
    this.lastTime,
    this.typeIndex,
    this.isLikeNum,
    this.limitJoinNum,
    this.joinUserList,
    this.evaluateList,
  });

  final int id; // 帖子ID
  final String title;
  final int authorID; // 作者ID
  final String mainAcrticle;
  final String createTime; // 创建时间
  final String lastTime;
  final int typeIndex; // 请到对应的type数据模型查找
  int isLikeNum; // 文章点赞数量
  final int limitJoinNum; // 限定参加的人数
  List joinUserList; // 参加的UserID的列表
  Map<int, double> evaluateList; // 已评价的userID列表，前者为userID，后者为对应的评价分数

  void addLikeNum() {
    this.isLikeNum += 1;
  }

  void reduceLikeNum() {
    this.isLikeNum -= 1;
  }

  // 用户加入活动
  void addJoin(int userID) {
    joinUserList.add(userID);
  }

  // 用户取消参加该活动
  void reduceJoin(int userID) {
    joinUserList.remove(userID);
  }

  // 往evaluateList添加数据，并返回成功与否
  bool addEvaluateList(int userID, double evaluate) {
    if (this.evaluateList == null) {
      this.evaluateList = new Map();
    }
    if (this.evaluateList.containsKey(userID)) {
      return false;
    }
    this.evaluateList[userID] = evaluate;
    return true;
  }
}

// 转换毫秒数为时间，请使用common_utils库
// DateTime.parse(String);

// 通过articleID获取文章
Article getArticle(int articleID) {
  Article article;
  articles.forEach((articleItem) {
    if (articleItem.id == articleID) {
      article = articleItem;
      return;
    }
  });

  return article;
}

List<Article> articles = [
  // Article(
  //   id: 0,
  //   title: 'Candy Shop',
  //   authorID: 0,
  //   mainAcrticle: '我将要去三山楼学习高数，召集一个小伙伴一起学习哦~',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 0,
  //   isLikeNum: 10,
  //   limitJoinNum: 5,
  //   joinUserList: [1, 3, 7, 8, 9],
  // ),
  // Article(
  //   id: 1,
  //   title: 'Childhood in a picture',
  //   authorID: 1,
  //   mainAcrticle:
  //       'Esse ut nulla velit reprehenderit veniam sint nostrud nulla exercitation ipsum. Officia deserunt aliquip aliquip excepteur eiusmod dolor. Elit amet ipsum labore sint occaecat dolore tempor officia irure voluptate ad. Veniam laboris deserunt aute excepteur sit deserunt dolor esse dolor velit sint nulla anim ut. Reprehenderit voluptate adipisicing culpa magna ea nulla ullamco consectetur. Cupidatat adipisicing consequat adipisicing sit consectetur dolor occaecat.',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 1,
  //   isLikeNum: 4,
  //   limitJoinNum: 5,
  //   joinUserList: [],
  // ),
  // Article(
  //   id: 2,
  //   title: 'Contained',
  //   authorID: 2,
  //   mainAcrticle:
  //       'Esse ut nulla velit reprehenderit veniam sint nostrud nulla exercitation ipsum. Officia deserunt aliquip aliquip excepteur eiusmod dolor. Elit amet ipsum labore sint occaecat dolore tempor officia irure voluptate ad. Veniam laboris deserunt aute excepteur sit deserunt dolor esse dolor velit sint nulla anim ut. Reprehenderit voluptate adipisicing culpa magna ea nulla ullamco consectetur. Cupidatat adipisicing consequat adipisicing sit consectetur dolor occaecat.',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 2,
  //   isLikeNum: 6,
  //   limitJoinNum: 5,
  //   joinUserList: [],
  // ),
  // Article(
  //   id: 3,
  //   title: 'Dragon',
  //   authorID: 3,
  //   mainAcrticle:
  //       'Esse ut nulla velit reprehenderit veniam sint nostrud nulla exercitation ipsum. Officia deserunt aliquip aliquip excepteur eiusmod dolor. Elit amet ipsum labore sint occaecat dolore tempor officia irure voluptate ad. Veniam laboris deserunt aute excepteur sit deserunt dolor esse dolor velit sint nulla anim ut. Reprehenderit voluptate adipisicing culpa magna ea nulla ullamco consectetur. Cupidatat adipisicing consequat adipisicing sit consectetur dolor occaecat.',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 3,
  //   isLikeNum: 3,
  //   limitJoinNum: 5,
  //   joinUserList: [],
  // ),
  // Article(
  //   id: 4,
  //   title: 'Free Hugs',
  //   authorID: 4,
  //   mainAcrticle:
  //       'Esse ut nulla velit reprehenderit veniam sint nostrud nulla exercitation ipsum. Officia deserunt aliquip aliquip excepteur eiusmod dolor. Elit amet ipsum labore sint occaecat dolore tempor officia irure voluptate ad. Veniam laboris deserunt aute excepteur sit deserunt dolor esse dolor velit sint nulla anim ut. Reprehenderit voluptate adipisicing culpa magna ea nulla ullamco consectetur. Cupidatat adipisicing consequat adipisicing sit consectetur dolor occaecat.',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 2,
  //   isLikeNum: 1,
  //   limitJoinNum: 5,
  //   joinUserList: [],
  // ),
  // Article(
  //   id: 5,
  //   title: 'Gravity Falls',
  //   authorID: 5,
  //   mainAcrticle:
  //       'Esse ut nulla velit reprehenderit veniam sint nostrud nulla exercitation ipsum. Officia deserunt aliquip aliquip excepteur eiusmod dolor. Elit amet ipsum labore sint occaecat dolore tempor officia irure voluptate ad. Veniam laboris deserunt aute excepteur sit deserunt dolor esse dolor velit sint nulla anim ut. Reprehenderit voluptate adipisicing culpa magna ea nulla ullamco consectetur. Cupidatat adipisicing consequat adipisicing sit consectetur dolor occaecat.',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 0,
  //   isLikeNum: 6,
  //   limitJoinNum: 5,
  //   joinUserList: [],
  // ),
  // Article(
  //   id: 6,
  //   title: 'Icecream Truck',
  //   authorID: 6,
  //   mainAcrticle:
  //       'Esse ut nulla velit reprehenderit veniam sint nostrud nulla exercitation ipsum. Officia deserunt aliquip aliquip excepteur eiusmod dolor. Elit amet ipsum labore sint occaecat dolore tempor officia irure voluptate ad. Veniam laboris deserunt aute excepteur sit deserunt dolor esse dolor velit sint nulla anim ut. Reprehenderit voluptate adipisicing culpa magna ea nulla ullamco consectetur. Cupidatat adipisicing consequat adipisicing sit consectetur dolor occaecat.',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 1,
  //   isLikeNum: 3,
  //   limitJoinNum: 5,
  //   joinUserList: [],
  // ),
  // Article(
  //   id: 7,
  //   title: 'keyclack',
  //   authorID: 7,
  //   mainAcrticle:
  //       'Esse ut nulla velit reprehenderit veniam sint nostrud nulla exercitation ipsum. Officia deserunt aliquip aliquip excepteur eiusmod dolor. Elit amet ipsum labore sint occaecat dolore tempor officia irure voluptate ad. Veniam laboris deserunt aute excepteur sit deserunt dolor esse dolor velit sint nulla anim ut. Reprehenderit voluptate adipisicing culpa magna ea nulla ullamco consectetur. Cupidatat adipisicing consequat adipisicing sit consectetur dolor occaecat.',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 1,
  //   isLikeNum: 2,
  //   limitJoinNum: 5,
  //   joinUserList: [],
  // ),
  // Article(
  //   id: 8,
  //   title: 'Overkill',
  //   authorID: 8,
  //   mainAcrticle:
  //       'Esse ut nulla velit reprehenderit veniam sint nostrud nulla exercitation ipsum. Officia deserunt aliquip aliquip excepteur eiusmod dolor. Elit amet ipsum labore sint occaecat dolore tempor officia irure voluptate ad. Veniam laboris deserunt aute excepteur sit deserunt dolor esse dolor velit sint nulla anim ut. Reprehenderit voluptate adipisicing culpa magna ea nulla ullamco consectetur. Cupidatat adipisicing consequat adipisicing sit consectetur dolor occaecat.',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 2,
  //   isLikeNum: 7,
  //   limitJoinNum: 5,
  //   joinUserList: [],
  // ),
  // Article(
  //   id: 9,
  //   title: 'Say Hello to Barry',
  //   authorID: 9,
  //   mainAcrticle:
  //       'Esse ut nulla velit reprehenderit veniam sint nostrud nulla exercitation ipsum. Officia deserunt aliquip aliquip excepteur eiusmod dolor. Elit amet ipsum labore sint occaecat dolore tempor officia irure voluptate ad. Veniam laboris deserunt aute excepteur sit deserunt dolor esse dolor velit sint nulla anim ut. Reprehenderit voluptate adipisicing culpa magna ea nulla ullamco consectetur. Cupidatat adipisicing consequat adipisicing sit consectetur dolor occaecat.',
  //   createTime: "1562485927175",
  //   lastTime: "1562485927175",
  //   typeIndex: 1,
  //   isLikeNum: 1,
  //   limitJoinNum: 5,
  //   joinUserList: [],
  // ),
];
