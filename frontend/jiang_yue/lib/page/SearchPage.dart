import 'package:flutter/material.dart';
import 'package:jiang_yue/modal/Article.dart'; // 文章数据模型
import 'package:jiang_yue/home/PostShow.dart';
import 'package:jiang_yue/modal/User.dart';
import 'package:jiang_yue/utils/cache/cache.dart'; // 用户数据模型

class SearchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _SearchPageState();
  }
}

class _SearchPageState extends State<SearchPage> {
  List articleList = [];

  void updateArticleList(String value) {
    List temp = [];
    if (value.length != 0) {
      for (var i = 0; i < articleInfos.length; i++) {
        if (articleInfos[i]['postTitle'].contains(value)) {
          temp.add(articleInfos[i]);
        } else if (articleInfos[i]['content'].contains(value)) {
          temp.add(articleInfos[i]);
        }
      }
    }

    setState(() {
      articleList = temp;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: TextFileWidget(callback: updateArticleList),
          automaticallyImplyLeading: false,
        ),
        body: SearchContent(
          articleList: articleList,
        ));
  }
}

class SearchContent extends StatelessWidget {
  final List articleList;
  User user;

  SearchContent({Key key, this.articleList}) : super(key: key);

  Widget _listItemBuilder(BuildContext context, int index) {
    return Stack(
      children: <Widget>[
        Container(
            color: Colors.white,
            margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
            child: Column(
              children: <Widget>[
                SizedBox(height: 8,),
                Text(
                  articleList[index]['postTitle'],
                  style: Theme.of(context).textTheme.title,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  articleList[index]['content'],
                  style: Theme.of(context).textTheme.subhead,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 10,
                ),
                Divider(
                  height: 1,
                ),
              ],
            )),
        Positioned.fill(
          child: Material(
            color: Colors.transparent,
            child: InkWell(
                splashColor: Colors.grey.withOpacity(0.3), // 慢慢展开的颜色
                highlightColor: Colors.grey.withOpacity(0.1), // 高亮颜色
                onTap: () {
                  user = getIsLoginUser();
                  if (user == null) {
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new PostShow(
                                articleID: articleList[index]['postID'],
                                index: index,
                              )),
                    );
                    return;
                  }

                  if (user.likeArticleList.contains(index)) {
                    // 如果用户喜欢的文章中有该文章的话
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new PostShow(
                                articleID: articleList[index]['postID'],
                                index: index,
                              )),
                    );
                  } else {
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new PostShow(
                                articleID: articleList[index]['postID'],
                                index: index,
                              )),
                    );
                  }
                }),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // 如果没有搜索结果，则显示“暂无搜索结果”
    if (articleList.length != 0) {
      return Container(
        margin: EdgeInsets.only(top: 10),
        child: ListView.builder(
          itemCount: articleList.length,
          itemBuilder: _listItemBuilder,
        ),
      );
    } else {
      return Center(
        child: Text(
          '暂无搜索结果',
          style: TextStyle(fontSize: 20, letterSpacing: 10, color: Colors.grey),
        ),
      );
    }
  }
}

///搜索控件widget
class TextFileWidget extends StatelessWidget {
  final callback;

  TextFileWidget({Key key, this.callback}) : super(key: key);

  Widget buildTextField() {
    //theme设置局部主题
    return TextField(
      cursorColor: Colors.white, //设置光标
      onChanged: (value) {
        callback(value);
      },
      decoration: InputDecoration(
          //输入框decoration属性
          contentPadding: new EdgeInsets.only(left: 0.0),
          //  fillColor: Colors.white,
          border: InputBorder.none,
          icon: Icon(Icons.search),
          hintText: "搜索",
          hintStyle: new TextStyle(fontSize: 18, color: Colors.white)),
      style: new TextStyle(fontSize: 18, color: Colors.white),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget editView() {
      return Container(
        //修饰黑色背景与圆角
        decoration: new BoxDecoration(
          border: Border.all(color: Colors.grey, width: 1.0), //灰色的一层边框
          color: Colors.grey,
          borderRadius: new BorderRadius.all(new Radius.circular(15.0)),
        ),
        alignment: Alignment.center,
        height: 36,
        padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
        child: buildTextField(),
      );
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: editView(),
          flex: 1,
        ),
        Padding(
          padding: const EdgeInsets.only(
            left: 5.0,
          ),
          child: GestureDetector(
            child: Text("取消"),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        )
      ],
    );
  }
}
