import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jiang_yue/home/StarShow.dart';
import 'package:jiang_yue/home/Toast.dart';
import 'package:jiang_yue/modal/Article.dart';
import 'package:jiang_yue/modal/User.dart';
import 'package:jiang_yue/utils/cache/UserCache.dart';
import 'package:jiang_yue/utils/cache/cache.dart';
import 'package:jiang_yue/utils/toast/TsUtils.dart';

class Evaluate extends StatefulWidget {
  @required
  final int articleID;
  final int userID;

  Evaluate({Key key, this.articleID, this.userID}) : super(key: key);
  @override
  _EvaluateState createState() => _EvaluateState();
}

class _EvaluateState extends State<Evaluate> {
  int totalEvaluate = 0;

  void handleData(int evaluate, int index) {
    print('index: $index');
    starList[index]["userID"] = evaluateList[index]['userID'];
    starList[index]["data"] = evaluate;

    print('xxxxxxxxxxxxx');
    print(starList);
  }

  void handleTotalData(int evaluate, int index) {
    setState(() {
      totalEvaluate = evaluate;
    });
  }

  Widget buildPosts(List joinUserList) {
    return ListView.builder(
      shrinkWrap: true,
      physics: new NeverScrollableScrollPhysics(),
      itemCount: joinUserList.length,
      itemBuilder: (BuildContext context, int index) {
        print(joinUserList[index]);
        return Container(
          padding: EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                      height: 60,
                      width: 60,
                      child: CircleAvatar(
                        backgroundImage: AssetImage(
                          joinUserList[index]['avatorURL']
                            ),
                      )),
                  SizedBox(
                    width: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        joinUserList[index]['nickname'],
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        joinUserList[index]['decoration'],
                        style: TextStyle(fontSize: 12, color: Colors.grey),
                      ),
                    ],
                  ),
                ],
              ),
              StarShow(
                index: index,
                callback: handleData,
              )
            ],
          ),
        );
      },
    );
  }

  // 提交用户评价
  void _handleSubmitEvaluate() async {
    print('userID: ${widget.userID}');
    print('articleID: ${widget.articleID}');

    Response response;
    response = await Dio().post("http://47.93.201.127:8080/jiangyue/submitEvaluate", data: {
      "userID": widget.userID, // 当前用户
      "articleID": widget.articleID, // 评价的帖子ID
      "totalEvaluate": totalEvaluate, // 总的评分
      "evaluate": starList
    });

    if (response.data['success']) {
      TsUtils.showShort('评价成功');
    } else {
      Toast.show(context, response.data['msg']);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (evaluateList.length == 0) {
      return Scaffold(
          appBar: AppBar(
            title: Text('评价'),
          ),
          body: Center(
            child: Text(
              '这个活动没有人参加',
              style: TextStyle(color: Colors.grey, fontSize: 30, letterSpacing: 10),
            ),
          ));
    }

    starList = List.filled(evaluateList.length, {"userID": null, "data": 0});

    return Scaffold(
        appBar: AppBar(
          title: Text('评价'),
          actions: <Widget>[
            Container(
              padding: EdgeInsets.only(right: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: _handleSubmitEvaluate,
                    child: Text(
                      '提交',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: <Widget>[
            buildPosts(evaluateList),
            SizedBox(
              height: 20,
            ),
            Container(
              padding: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      SizedBox(
                        width: 20,
                      ),
                      Column(
                        children: <Widget>[
                          SizedBox(
                            height: 18,
                          ),
                          Text(
                            '总体评价',
                            style: TextStyle(fontSize: 20),
                          ),
                        ],
                      ),
                    ],
                  ),
                  StarShow(
                    callback: handleTotalData,
                    index: 6,
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
