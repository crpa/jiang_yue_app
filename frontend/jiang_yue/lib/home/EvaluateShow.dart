import 'package:common_utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:jiang_yue/modal/Article.dart';

class EvaluateShow extends StatefulWidget {
  final int evaluate;

  EvaluateShow({Key key, this.evaluate}) : super(key: key);
  @override
  _EvaluateShowState createState() => _EvaluateShowState();
}

class _EvaluateShowState extends State<EvaluateShow> {
  @override
  Widget build(BuildContext context) {
    print('显示评分');
    print(widget.evaluate);
    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 30,
            width: 50,
            decoration: new BoxDecoration(
              border: new Border.all(color: Colors.grey, width: 0.5),
              borderRadius: new BorderRadius.circular((5.0)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  (widget.evaluate != null ? '${widget.evaluate} 分' : '未评价'),
                  style: TextStyle(color: Colors.yellow[800]),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
