import 'package:flutter/material.dart';
import 'package:jiang_yue/home/Toast.dart';
import 'package:jiang_yue/utils/HttpUtils.dart';
import 'package:jiang_yue/utils/cache/UserCache.dart';
import 'package:jiang_yue/utils/cache/cache.dart';
import 'package:jiang_yue/utils/toast/TsUtils.dart';

class ShowMessage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('我的消息'),
      ),
      body: ShowMessageHome(),
    );
  }
}

class ShowMessageHome extends StatefulWidget {
  @override
  _ShowMessageHomeState createState() => _ShowMessageHomeState();
}

class _ShowMessageHomeState extends State<ShowMessageHome> {
  List messages = [];
  List commentMsg = [];

  @override
  Widget build(BuildContext context) {
    handleGetShelf();
    handleCommentMsg();

    if (messages.length == 0 && commentMsg.length == 0) {
      return Center(
        child: Text(
          '暂无消息',
          style: TextStyle(fontSize: 30, letterSpacing: 20, color: Colors.grey),
        ),
      );
    }

    Widget buildFriendMsg(List message) {
      if (message.length == 0) {
        return Container();
      }

      List<Widget> item = [];

      message.forEach((messageItem) {
        item.add(Column(
          children: <Widget>[
            Container(
                width: 400,
                margin: EdgeInsets.fromLTRB(4, 0, 8, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          children: <Widget>[
                            SizedBox(
                              width: 16,
                            ),
                            Container(
                                height: 50,
                                width: 50,
                                child: CircleAvatar(
                                  backgroundImage: AssetImage(
                                    messageItem['avatorURL'],
                                  ),
                                  radius: 25,
                                )),
                            SizedBox(
                              width: 16,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  messageItem['nickname'],
                                  style: TextStyle(fontSize: 18),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  '该用户请求添加你为好友',
                                  style: TextStyle(color: Colors.grey, fontSize: 12),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 16,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            _agree(messageItem['friendID']);
                            setState(() {});
                          },
                          child: Text('同意'),
                          elevation: 3,
                          textColor: Theme.of(context).accentColor,
                        ),
                        SizedBox(
                          width: 16,
                        ),
                      ],
                    )
                  ],
                )),
            Divider(
              height: 1,
            ),
          ],
        ));
      });

      return Column(
        children: item,
      );
    }

    Widget buildCommentMsg(List message) {
      if (message.length == 0) {
        return Container();
      }

      List<Widget> item = [];

      message.forEach((messageItem) {
        print(messageItem);
        item.add(Column(
          children: <Widget>[
            Container(
                width: 400,
                margin: EdgeInsets.fromLTRB(4, 0, 8, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          children: <Widget>[
                            SizedBox(
                              width: 16,
                            ),
                            Container(
                                height: 50,
                                width: 50,
                                child: CircleAvatar(
                                  backgroundImage: AssetImage(
                                    messageItem['avatorURL'],
                                  ),
                                  radius: 25,
                                )),
                            SizedBox(
                              width: 16,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  messageItem['nickname'],
                                  style: TextStyle(fontSize: 18),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  messageItem['content'],
                                  style: TextStyle(color: Colors.grey, fontSize: 12),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 16,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          '有人评论了你',
                          style: TextStyle(fontSize: 20, color: Colors.grey),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                      ],
                    )
                  ],
                )),
            Divider(
              height: 1,
            ),
          ],
        ));
      });

      return Column(
        children: item,
      );
    }

    return ListView(
      shrinkWrap: true, //解决无限高度问题
      physics: new NeverScrollableScrollPhysics(), //禁用滑动事件
      children: <Widget>[buildFriendMsg(messages), buildCommentMsg(commentMsg)],
    );
  }

  void handleGetShelf() async {
    var result =
        await HttpUtils.request('http://47.93.201.127:8080/jiangyue/friendMessage', method: HttpUtils.POST, data: {
      'userID': userInfo[0].id,
    });
    if (result['success'] == true) {
      messageList = result['data']; //将数据放入cache.dart缓存
      if ((messages.length != messageList.length)) {
        setState(() {
          messages = messageList;
        });
      }
    }
  }

  void handleCommentMsg() async {
    var result =
        await HttpUtils.request('http://47.93.201.127:8080/jiangyue/commentMessage', method: HttpUtils.GET, data: {
      'userID': userInfo[0].id,
    });
    if (result['success'] == true) {
      commentMsgList = result['data']; //将数据放入cache.dart缓存
      if ((commentMsgList.length != commentMsg.length)) {
        setState(() {
          commentMsg = commentMsgList;
        });
      }
    }
  }

  _agree(int friendID) async {
    var result = await HttpUtils.request('http://47.93.201.127:8080/jiangyue/addFriend',
        method: HttpUtils.POST, data: {'userID': userInfo[0].id, "addFriendID": friendID});
    if (result['success'] == true) {
      TsUtils.showShort('添加成功');
    }
  }
}
