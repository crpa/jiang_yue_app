package com.sarracenia.jiangyue.model.vo;

import lombok.Data;
import org.dozer.Mapping;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Sarracenia
 * @date 2020/2/13 0:39
 */
@Data
public class AddPostVO {
    @NotNull
    @Mapping("userId")
    Integer userID;
    @NotNull
    String title;
    @NotNull
    String content;
    @NotNull
    @Mapping("tagid")
    Integer tagID;
    @NotNull
    Date lastTime;
    @NotNull
    Integer limitJoinNum;


}
