package com.sarracenia.jiangyue.model.entity;

public class FocusList {
    private Integer id;

    private Integer focusUserid;

    private Integer focusedUserid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFocusUserid() {
        return focusUserid;
    }

    public void setFocusUserid(Integer focusUserid) {
        this.focusUserid = focusUserid;
    }

    public Integer getFocusedUserid() {
        return focusedUserid;
    }

    public void setFocusedUserid(Integer focusedUserid) {
        this.focusedUserid = focusedUserid;
    }
}