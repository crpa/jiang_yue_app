package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @author Sarracenia
 * @date 2020/2/17 2:38
 */
@Data
public class EvaluateActivityVO {
    Integer userID;
    Integer articleID;
    Integer totalEvaluate;
    List<UserEvaluateVO> evaluate;

}
