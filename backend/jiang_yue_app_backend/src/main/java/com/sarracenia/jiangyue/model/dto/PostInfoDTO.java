package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

import java.util.Date;

/**
 * @author Sarracenia
 * @date 2020/2/18 3:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostInfoDTO {
    @Mapping("postId")
    Integer articleID;
    Integer authorID;
    String avatorURL;
    String decoration;
    Date lastTime;
    String content;
    @Mapping("likenum")
    Integer likeNum;
    Integer commentNum;
    Integer limitJoinNum;
    Integer evaluate;
    boolean isLike;
    Integer joinNum;
    boolean join;
}
