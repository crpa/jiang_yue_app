package com.sarracenia.jiangyue.model.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/12 23:51
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetPostVO {
    @NotNull
    Integer articleID;
    Integer userID;
}
