package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/16 16:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetFriendApplicationDTO {
    Integer friendID;
    Integer userID;
    String nickname;
    String avatorURL;
}
