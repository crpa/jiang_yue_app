package com.sarracenia.jiangyue.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/17 16:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetEvaluateUserListVO {
     Integer userID;
     Integer articleID;
}
