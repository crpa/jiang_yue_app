package com.sarracenia.jiangyue.service;

import com.sarracenia.jiangyue.exception.GlobalException;
import com.sarracenia.jiangyue.model.dto.*;
import com.sarracenia.jiangyue.model.vo.*;

import java.util.List;

/**
 * @author Sarracenia
 * @date 2020/2/11 16:58
 */
public interface UserService {

    //TODO  赶时间需求接口老变动详细注释就不写了
    //login登录
    LoginDTO login(LoginVO loginVO) throws GlobalException;

    //注册
    EnrollDTO enroll(EnrollVO enrollVO) throws GlobalException;

    //登出
    LogoutDTO logout(LogoutVO logoutVO) throws GlobalException;

    //获取评论过的帖子list
    List<PostBriefInfoDTO> getCommentedPosts(GetCommentedPostVO getCommentedPostVO) throws GlobalException;

    //我的消息(评论回复)
    List<CommentBriefInfoDTO> getCommentMessage(GetMyMessageVO getMyMessageVO) throws GlobalException;

    //获取浏览过的帖子list
    List<PostBriefInfoDTO> getBrowsedPosts(GetBrowsedPostVO getBrowsedPostVO) throws GlobalException;

    //获取发布过的帖子list
    List<PostBriefInfoDTO> getPublishedPosts(GetPublishedPostVO getPublishedPostVO) throws GlobalException;

    //获取参加过活动的帖子list
    List<PostBriefInfoDTO> getJoinedPosts(GetJoinedPostVO getJoinedPostVO) throws GlobalException;

    //查询朋友
    //TODO 后期可以参考elasticsearch的分词做模糊查询
    List<FriendInfoDTO> searchFriend(SearchFriendVO searchFriendVO) throws GlobalException;

    //获取添加好友请求
    List<GetFriendApplicationDTO> getFriendApplication(GetFriendApplicationVO getFriendApplicationVO) throws GlobalException;

    //增加朋友消息(用户是否存在+重复添加好友)
    void addFriend(AddFriendVO addFriendVO) throws GlobalException;

    //确认添加朋友(用户是否存在+重复添加好友)
    void confirmFriendAdd(ConfirmFriendAddVO confirmFriendAddVO) throws GlobalException;

    //获取用户信息
    GetUserInfoDTO getUserInfo(GetUserInfoVO getUserInfoVO) throws GlobalException;

    //我的界面(编辑界面)
    EditUserInfoDTO editUserInfo(EditUserInfoVO editUserInfoVO) throws GlobalException;

    //获取帖子列表(home首页界面)
    PostHomeListDTO getPostList(GetPostListVO getPostListVO) throws GlobalException;

    //点赞或者是不点赞 记得同时维护update post的likenum字段和like表
    LikePostDTO likePost(LikePostVO likePostVO) throws GlobalException;

    //查看帖子（点击）
    GetPostDTO getPostInfo(GetPostVO getPostVO) throws GlobalException;

    //发起帖子（add）
    AddPostDTO addPost(AddPostVO addPostVO) throws GlobalException;

    //删除帖子
    DeletePostDTO deletePost(DeletePostVO deletePostVO) throws GlobalException;

    //加入活动
    JoinActivityDTO joinPost(JoinActivityVO joinActivityVO) throws GlobalException;

    //发表评论
    AddCommentDTO addComment(AddCommentVO addCommentVO) throws GlobalException;


    //提交活动结束后评价
    EvaluateActivityDTO evaluateActivity(EvaluateActivityVO evaluateActivityVO) throws GlobalException;


    //显示当前活动结束后可被评价的用户
    List<GetEvaluateUserDTO> getEvaluateUserList(GetEvaluateUserListVO getEvaluateUserListVO) throws GlobalException;

    //获取好友列表
    List<FriendInfoDTO> getFriendList(GetFriendListVO getFriendListVO) throws GlobalException;


}
