package com.sarracenia.jiangyue.model.entity;

import java.util.ArrayList;
import java.util.List;

public class FriendApplicationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FriendApplicationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHandledIsNull() {
            addCriterion("handled is null");
            return (Criteria) this;
        }

        public Criteria andHandledIsNotNull() {
            addCriterion("handled is not null");
            return (Criteria) this;
        }

        public Criteria andHandledEqualTo(Boolean value) {
            addCriterion("handled =", value, "handled");
            return (Criteria) this;
        }

        public Criteria andHandledNotEqualTo(Boolean value) {
            addCriterion("handled <>", value, "handled");
            return (Criteria) this;
        }

        public Criteria andHandledGreaterThan(Boolean value) {
            addCriterion("handled >", value, "handled");
            return (Criteria) this;
        }

        public Criteria andHandledGreaterThanOrEqualTo(Boolean value) {
            addCriterion("handled >=", value, "handled");
            return (Criteria) this;
        }

        public Criteria andHandledLessThan(Boolean value) {
            addCriterion("handled <", value, "handled");
            return (Criteria) this;
        }

        public Criteria andHandledLessThanOrEqualTo(Boolean value) {
            addCriterion("handled <=", value, "handled");
            return (Criteria) this;
        }

        public Criteria andHandledIn(List<Boolean> values) {
            addCriterion("handled in", values, "handled");
            return (Criteria) this;
        }

        public Criteria andHandledNotIn(List<Boolean> values) {
            addCriterion("handled not in", values, "handled");
            return (Criteria) this;
        }

        public Criteria andHandledBetween(Boolean value1, Boolean value2) {
            addCriterion("handled between", value1, value2, "handled");
            return (Criteria) this;
        }

        public Criteria andHandledNotBetween(Boolean value1, Boolean value2) {
            addCriterion("handled not between", value1, value2, "handled");
            return (Criteria) this;
        }

        public Criteria andFromUseridIsNull() {
            addCriterion("from_userid is null");
            return (Criteria) this;
        }

        public Criteria andFromUseridIsNotNull() {
            addCriterion("from_userid is not null");
            return (Criteria) this;
        }

        public Criteria andFromUseridEqualTo(Integer value) {
            addCriterion("from_userid =", value, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andFromUseridNotEqualTo(Integer value) {
            addCriterion("from_userid <>", value, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andFromUseridGreaterThan(Integer value) {
            addCriterion("from_userid >", value, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andFromUseridGreaterThanOrEqualTo(Integer value) {
            addCriterion("from_userid >=", value, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andFromUseridLessThan(Integer value) {
            addCriterion("from_userid <", value, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andFromUseridLessThanOrEqualTo(Integer value) {
            addCriterion("from_userid <=", value, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andFromUseridIn(List<Integer> values) {
            addCriterion("from_userid in", values, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andFromUseridNotIn(List<Integer> values) {
            addCriterion("from_userid not in", values, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andFromUseridBetween(Integer value1, Integer value2) {
            addCriterion("from_userid between", value1, value2, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andFromUseridNotBetween(Integer value1, Integer value2) {
            addCriterion("from_userid not between", value1, value2, "fromUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridIsNull() {
            addCriterion("to_userid is null");
            return (Criteria) this;
        }

        public Criteria andToUseridIsNotNull() {
            addCriterion("to_userid is not null");
            return (Criteria) this;
        }

        public Criteria andToUseridEqualTo(Integer value) {
            addCriterion("to_userid =", value, "toUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridNotEqualTo(Integer value) {
            addCriterion("to_userid <>", value, "toUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridGreaterThan(Integer value) {
            addCriterion("to_userid >", value, "toUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridGreaterThanOrEqualTo(Integer value) {
            addCriterion("to_userid >=", value, "toUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridLessThan(Integer value) {
            addCriterion("to_userid <", value, "toUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridLessThanOrEqualTo(Integer value) {
            addCriterion("to_userid <=", value, "toUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridIn(List<Integer> values) {
            addCriterion("to_userid in", values, "toUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridNotIn(List<Integer> values) {
            addCriterion("to_userid not in", values, "toUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridBetween(Integer value1, Integer value2) {
            addCriterion("to_userid between", value1, value2, "toUserid");
            return (Criteria) this;
        }

        public Criteria andToUseridNotBetween(Integer value1, Integer value2) {
            addCriterion("to_userid not between", value1, value2, "toUserid");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}