package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/16 16:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentInfoDTO {
    Integer id;
    Integer authorID;
    String author;
    String decoration;
    String content;
    String avatorURL;

}
