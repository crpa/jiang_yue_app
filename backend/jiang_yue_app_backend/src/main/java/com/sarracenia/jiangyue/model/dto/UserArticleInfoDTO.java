package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

/**
 * @author Sarracenia
 * @date 2020/2/19 20:57
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserArticleInfoDTO {
    @Mapping("postId")
    Integer articleID;
    String title;
    String author;// 作者
    String content;//30字

}
