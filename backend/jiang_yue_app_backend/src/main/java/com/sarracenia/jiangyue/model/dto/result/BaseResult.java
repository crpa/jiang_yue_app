package com.sarracenia.jiangyue.model.dto.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResult<T> implements Serializable {
    private int code;
    private T data;
    private String msg;
    private boolean success;

    private BaseResult(T data){
        this.code = CodeMessage.CODE_SUCCESS;
        this.data = data;
    }
    private BaseResult(T data,boolean success){
        this.code = CodeMessage.CODE_SUCCESS;
        this.data = data;
        this.success=success;
    }

    private BaseResult(CodeMessage codeMessage){
        this.code = codeMessage.getCode();
        this.msg = codeMessage.getMessage();
    }

    private BaseResult(CodeMessage codeMessage,boolean success){
        this.code = codeMessage.getCode();
        this.msg = codeMessage.getMessage();
        this.success=success;

    }

    public static <T> BaseResult<T> success(T data){ return new BaseResult<>(data,true);
    }

    public static <T> BaseResult<T> fail(CodeMessage codeMessage){
        return new BaseResult<>(codeMessage,false);
    }
}
