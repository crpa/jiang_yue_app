package com.sarracenia.jiangyue.controller;

import com.sarracenia.jiangyue.exception.GlobalException;
import com.sarracenia.jiangyue.model.dto.*;
import com.sarracenia.jiangyue.model.dto.result.BaseResult;
import com.sarracenia.jiangyue.model.vo.*;
import com.sarracenia.jiangyue.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Sarracenia
 * @date 2020/2/22 0:05
 */
@RestController
public class PostController {
    @Autowired
    UserService userService;

    @GetMapping("/postList")
    public BaseResult<PostHomeListDTO> getPostList(GetPostListVO getPostListVO) throws GlobalException {

        return BaseResult.success(userService.getPostList(getPostListVO));
    }

    @GetMapping("/postShow")
    public BaseResult<GetPostDTO> getPostInfo(GetPostVO getPostVO) throws GlobalException {
        return BaseResult.success(userService.getPostInfo(getPostVO));
    }

    //点赞 取消点赞
    @PostMapping("/like")
    public BaseResult<LikePostDTO> likePost(@RequestBody @Valid LikePostVO likePostVO) throws GlobalException {

        return BaseResult.success(userService.likePost(likePostVO));
    }

    //发表评论
    @PostMapping("/addComment")
    public BaseResult<AddCommentDTO> addComment(@RequestBody @Valid AddCommentVO addCommentVO) throws GlobalException {

        return BaseResult.success(userService.addComment(addCommentVO));
    }

    //发表帖子
    @PostMapping("/publishPost")
    public BaseResult<AddPostDTO> addPost(@RequestBody @Valid AddPostVO addPostVO) throws GlobalException {
        return BaseResult.success(userService.addPost(addPostVO));
    }

    //评论过的帖子 [GET] commentPosts
    @GetMapping("/commentPosts")
    public BaseResult<List<PostBriefInfoDTO>> getCommentedPosts(@Valid GetCommentedPostVO getCommentedPostVO) throws GlobalException {

        return BaseResult.success(userService.getCommentedPosts(getCommentedPostVO));

    }

    //[GET] /browsePosts
    //浏览过的帖子
    @GetMapping("/browsePosts")
    public BaseResult<List<PostBriefInfoDTO>> getBrowsedPosts(@Valid GetBrowsedPostVO getBrowsedPostVO) throws GlobalException {
        return BaseResult.success(userService.getBrowsedPosts(getBrowsedPostVO));

    }


    //获取发布过的帖子
    //[GET] /publishPosts
    @GetMapping("/publishPosts")
    public BaseResult<List<PostBriefInfoDTO>> getPublishedPosts(@Valid GetPublishedPostVO getPublishedPostVO) throws GlobalException {
        return BaseResult.success(userService.getPublishedPosts(getPublishedPostVO));

    }

    //参加过活动的帖子
    //[GET] /joinPosts
    @GetMapping("/joinPosts")
    public BaseResult<List<PostBriefInfoDTO>> getJoinedPosts(@Valid GetJoinedPostVO getJoinedPostVO) throws GlobalException {
        return BaseResult.success(userService.getJoinedPosts(getJoinedPostVO));

    }


    //获取评论   获取别人在当前用户发布的帖子上发表的评论
    //[GET] /commentMessage
    @GetMapping("/commentMessage")
    public BaseResult<List<CommentBriefInfoDTO>> getCommentMessage(@Valid GetMyMessageVO getMyMessageVO) throws GlobalException {
        return BaseResult.success(userService.getCommentMessage(getMyMessageVO));

    }
}
