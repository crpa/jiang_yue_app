package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/15 18:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentBriefInfoDTO {
     Integer commentID;
     Integer userID;
     String nickname;
     String avatorURL;
     String content;
}
