package com.sarracenia.jiangyue.controller;

import com.sarracenia.jiangyue.exception.GlobalException;
import com.sarracenia.jiangyue.model.dto.*;
import com.sarracenia.jiangyue.model.dto.result.BaseResult;
import com.sarracenia.jiangyue.model.dto.result.CodeMessage;
import com.sarracenia.jiangyue.model.vo.*;
import com.sarracenia.jiangyue.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

/**
 * @author Sarracenia
 * @date 2020/2/11 17:43
 */
@RestController
public class UserController {
    @Autowired
    UserService userService;


    @PostMapping("/login")
    public BaseResult<LoginDTO> login(@RequestBody @Valid LoginVO loginVO) throws GlobalException {
        return BaseResult.success(userService.login(loginVO));
    }

    @PostMapping("/enroll")
    public BaseResult<EnrollDTO> enroll(@RequestBody @Valid EnrollVO enrollVO) throws GlobalException {
        System.out.println(enrollVO.getPassword());
        System.out.println(enrollVO.getUsername());
        System.out.println(enrollVO.getRepassword());
        if (!enrollVO.getPassword().equals(enrollVO.getRepassword())) {
            throw new GlobalException(CodeMessage.ERROR_REPASSWORD_PASSWORD);
        }
        return BaseResult.success(userService.enroll(enrollVO));
    }

    @PostMapping("/exit")
    public BaseResult<LogoutDTO> exit(@RequestBody @Valid LogoutVO logoutVO) throws GlobalException {
        return BaseResult.success(userService.logout(logoutVO));
    }


    @PostMapping("/update")
    public BaseResult<EditUserInfoDTO> editUserInfo(@RequestBody @Valid EditUserInfoVO editUserInfoVO) throws GlobalException {

        return BaseResult.success(userService.editUserInfo(editUserInfoVO));
    }


    //参加活动
    @PostMapping("/join")
    public BaseResult<JoinActivityDTO> joinPost(@RequestBody @Valid JoinActivityVO joinActivityVO) throws GlobalException {

        return BaseResult.success(userService.joinPost(joinActivityVO));
    }


    //查询朋友
    @GetMapping("/showFriends")
    public BaseResult<List<FriendInfoDTO>> searchFriend(@Valid SearchFriendVO searchFriendVO) throws GlobalException {

        return BaseResult.success(userService.searchFriend(searchFriendVO));
    }

    //[POST] /addFriendMessage 增加朋友消息
    @PostMapping("/addFriendMessage")
    public BaseResult<List<FriendInfoDTO>> addFriendMessage(@RequestBody @Valid AddFriendVO addFriendVO) throws GlobalException {
        userService.addFriend(addFriendVO);
        return BaseResult.success(null);
    }

    //确认添加朋友 [POST]
    @PostMapping("/addFriend")
    public BaseResult<List<FriendInfoDTO>> confirmFriendAdd(@RequestBody @Valid ConfirmFriendAddVO confirmFriendAddVO) throws GlobalException {

        userService.confirmFriendAdd(confirmFriendAddVO);

        return BaseResult.success(null);

    }


    //[POST] /friendMessage
    // 获取好友请求
    @PostMapping("/friendMessage")
    public BaseResult<List<GetFriendApplicationDTO>> getFriendApplication(@RequestBody @Valid GetFriendApplicationVO getFriendApplicationVO) throws GlobalException {
        return BaseResult.success(userService.getFriendApplication(getFriendApplicationVO));

    }


    //获取评论   获取别人在当前用户发布的帖子上发表的评论
    //[GET] /commentMessage
    @GetMapping("/commentMessage")
    public BaseResult<List<CommentBriefInfoDTO>> getCommentMessage(@Valid GetMyMessageVO getMyMessageVO) throws GlobalException {
        return BaseResult.success(userService.getCommentMessage(getMyMessageVO));

    }

    //提交评价
    //[POST] /submitEvaluate
    @PostMapping("/submitEvaluate")
    public BaseResult<EvaluateActivityDTO> evaluateActivity(@RequestBody @Valid EvaluateActivityVO evaluateActivityVO) throws GlobalException {
        return BaseResult.success(userService.evaluateActivity(evaluateActivityVO));

    }

    //TODO: 最终评价活动总评价的分数计算规则有待改进
    //显示评价的用户
    //[GET] /showEvaluateUsers
    @GetMapping("/showEvaluateUsers")
    public BaseResult<List<GetEvaluateUserDTO>> getEvaluateUserList(@Valid GetEvaluateUserListVO getEvaluateUserListVO) throws GlobalException {
        return BaseResult.success(userService.getEvaluateUserList(getEvaluateUserListVO));
    }

    @GetMapping("/friendList")
    public BaseResult<List<FriendInfoDTO>> getEvaluateUserList(@Valid GetFriendListVO getFriendListVO) throws GlobalException {
        return BaseResult.success(userService.getFriendList(getFriendListVO));
    }


    @GetMapping("/userShow")
    public BaseResult<GetUserInfoDTO> getUserInfo(@Valid GetUserInfoVO getUserInfoVO) throws GlobalException {
        return BaseResult.success(userService.getUserInfo(getUserInfoVO));
    }



























    /*
    @PostMapping("/postShow/look")
    public BaseResult<GetPostDTO> getPost(@RequestBody @Valid GetPostVO getPostVO) throws GlobalException {
        Date data=new Date();
        return BaseResult.success(userService.getPostInfo(getPostVO));
    }



    @PostMapping("/my/post/delet")
    public BaseResult<DeletePostDTO> deletePost(@RequestBody @Valid DeletePostVO deletePostVO) throws GlobalException {
        return BaseResult.success(userService.deletePost(deletePostVO));
    }

    @PostMapping("/postList")
    public BaseResult<GetUserInfoDTO> getUserInfo(@RequestBody @Valid GetUserInfoVO getUserInfoVO) throws GlobalException {

        return BaseResult.success(userService.getUserInfo(getUserInfoVO));
    }
    */
}
