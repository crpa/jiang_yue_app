package com.sarracenia.jiangyue.model.vo;

import lombok.Data;
import org.dozer.Mapping;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Sarracenia
 * @date 2020/2/11 18:30
 */
@Data
public class EnrollVO {

    @NotNull(message = "用户名不能为空")
    @NotEmpty(message = "用户名不能为空")
    String username;
    @NotNull(message = "密码不能为空")
    @NotEmpty(message = "密码不能为空")
    String password;
    @NotNull(message = "确认密码不能为空")
    @NotEmpty(message = "确认密码不能为空")
    String repassword;

}
