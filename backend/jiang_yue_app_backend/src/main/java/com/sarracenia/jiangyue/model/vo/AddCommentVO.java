package com.sarracenia.jiangyue.model.vo;

import lombok.Data;
import org.dozer.Mapping;

/**
 * @author Sarracenia
 * @date 2020/2/17 1:15
 */
@Data
public class AddCommentVO {
    @Mapping("commentedUserId")
    Integer userID;
    @Mapping("postId")
    Integer articleID;
    @Mapping("commentContent")
    String content;

}
