package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

/**
 * @author Sarracenia
 * @date 2020/2/17 16:06
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetEvaluateUserDTO {
    @Mapping("id")
    Integer userID;
    String nickname;
    @Mapping("avatorurl")
    String avatorURL;
    String decoration;
}
