package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/16 22:59
 */
@Data
public class LikePostVO {
    @NotNull
    Integer userID;
    @NotNull
    Integer articleID;
    @NotNull
    boolean like;
}
