package com.sarracenia.jiangyue.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/13 1:13
 */
@Data
public class DeletePostVO {
    @NotNull
    Integer userID;
    @NotNull
    Integer postID;
}
