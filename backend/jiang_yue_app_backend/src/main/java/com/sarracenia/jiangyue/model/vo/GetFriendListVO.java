package com.sarracenia.jiangyue.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/19 18:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetFriendListVO {
    Integer userID;
}
