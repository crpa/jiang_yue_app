package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sarracenia
 * @date 2020/2/18 3:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestDTO {
    boolean why;
}
