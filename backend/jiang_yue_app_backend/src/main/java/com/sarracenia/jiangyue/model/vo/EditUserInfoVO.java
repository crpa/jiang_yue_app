package com.sarracenia.jiangyue.model.vo;

import com.sarracenia.jiangyue.model.dto.GetUserInfoDTO;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/11 18:31
 */
@Data
public class EditUserInfoVO {

    @NotNull(message = "用户ID不能为空")
    int userID;
    @NotNull(message = "昵称不能为空")
    @NotEmpty(message = "昵称不能为空")
    String nickname;
    @NotNull(message = "头像不能为空")
    @NotEmpty(message = "头像不能为空")
    String avatorURL;
    @NotNull(message = "简介不能为空")
    String decoration;
}
