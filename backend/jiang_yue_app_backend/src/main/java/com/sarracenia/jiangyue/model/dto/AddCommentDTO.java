package com.sarracenia.jiangyue.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

/**
 * @author Sarracenia
 * @date 2020/2/17 1:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddCommentDTO {
    @Mapping("avatorurl")
    String avatorURL;
    @Mapping("nickname")
    String author;
    String decoration;
}
