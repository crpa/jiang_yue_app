package com.sarracenia.jiangyue.model.vo;

import com.sarracenia.jiangyue.model.dto.GetUserInfoDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Sarracenia
 * @date 2020/2/11 18:31
 */
@Data
public class GetUserInfoVO {
    @NotNull
    Integer userID;

}
